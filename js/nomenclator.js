//=========================== Nomenclators =====================================


function populateNomenclator(table, jsonArray, canInsertId)
{
  populateNomenclator(table, jsonArray, canInsertId, null);
}

function populateNomenclator(table, jsonArray, canInsertId, additionalHtml)
{
  var tableId = table.attr('id');

  //Iterate jsonArray and append a TR in table
  $.each(jsonArray, 
    function(index, value)
    {      
      var tr            = $('<tr id="'+tableId+'_'+index+'">');
      
      var tdIndex       = $('<td class="identity">'+index+'</td>');
      var tdName        = $('<td class="name"><span>'+value+'</span></td>');
      var tdOperations  = $('<td class="operations">'+(additionalHtml!=null ? additionalHtml : '')+'</td>');    
      
      tdOperations.append('<img src="../img/btn_edit.png" width="12" height="12" alt="" title="Editeaza entitate" onclick="editFromNom($(this).parent().parent()); return false;">');
      tdOperations.append('<img src="../img/btn_delete.png" width="12" height="12" alt="" title="Sterge entitate" onclick="delFromNom($(this).parent().parent()); return false;">'); 
       
      tr.append(tdIndex);
      tr.append(tdName);
      tr.append(tdOperations);
      
      table.append(tr);
        
    }
  );
  
  //Create a loading bar which is going to be visible only when AJAX is performing
  var loadingBar = $('<img id="'+tableId+'_loadingBar" src="../img/loading_bar.gif" width="50" height="10" alt="loading...">');
  $('#'+tableId+' th').append(loadingBar);
  loadingBar.hide();
    
  //Create the special TR for inserting new entites
  var tr = $('<tr id="'+tableId+'_insert">');
  tr.css('background', '#dbdbdb');
  
  var tdIndex       = $('<td class="identity"><input type="text" id="'+tableId+'_identity" size="3" maxlength="4" /></td>');  
  var tdName        = $('<td class="name"><input type="text" id="'+tableId+'name" size="10" maxlength="32" /></td>');
  var tdOperations  = $('<td class="operations">');    
  
  tdOperations.append('<img src="../img/btn_add.png" width="12" height="12" alt="" title="Adauga entitate" onclick="insertIntoNom($(this).parent().parent()); return false;">');  
  
  tr.append(tdIndex);
  tr.append(tdName);
  tr.append(tdOperations);
  
  table.append(tr);

  if (! canInsertId)
  {
    $('#'+tableId+' td.identity').css("color", "#dbdbdb");
    $('#'+tableId+'_identity').attr('disabled', true);  
    $('#'+tableId+'_identity').val('Auto');
  }
}





function insertIntoNom(tr)
{
  var trId          = tr.attr('id');  
  var table         = tr.parent().parent();  
  var tableId       = table.attr('id');  
  var eId           = $('#'+trId+' td.identity input').val();
  var eName         = $('#'+trId+' td.name input').val(); 
  var eIdParent     = table.attr("idParent"); 
  var trNew         = $('<tr id="'+tableId+'_'+eId+'">');
  
  var tdIndex       = $('<td class="identity">'+eId+'</td>');
  var tdName        = $('<td class="name"><span>'+eName+'</span></td>'); 
  var tdOperations  = $('<td class="operations"></td>'); 
  
  tdOperations.append('<img src="../img/btn_edit.png" width="12" height="12" alt="" title="Editeaza entitate" onclick="editFromNom($(this).parent().parent()); return false;">');
  tdOperations.append('<img src="../img/btn_delete.png" width="12" height="12" alt="" title="Sterge entitate" onclick="delFromNom($(this).parent().parent()); return false;">'); 

  trNew.append(tdIndex);
  trNew.append(tdName);
  trNew.append(tdOperations);

  if ( (eId.length<1 && eId!='Auto') || eName.length<1 )
    {
      alert('Introduceti valori mai intai, apoi apasati butonul de adaugare!');
    }
  else
    {
  		$.ajax({
    	  type: 'POST',
    	  url: '../ajax.php',
    	  data: ({act: 'act'+tableId, op: 1, id: eId, name: eName, idParent: eIdParent }),  //op: 1 = INSERT, idParent is optional (may be null her)
        beforeSend: function() { disableNomenclator(tableId, true); },
    	  success: function(h) 
        { 
          trNew.insertBefore(tr);
          if ($('#'+trId+' td.identity input').val()!='Auto')
          { 
            $('#'+trId+' td.identity input').val('');
          } 
          $('#'+trId+' td.name input').val(''); 
          disableNomenclator(tableId, false);
          if (! eIdParent)
            window.location.reload(); //Reload the page after inset is complete              
        }  
      });
    }  
}


function editFromNom(tr)
{
  var trId    = tr.attr('id');  
  var table   = tr.parent().parent();  
  var tableId = table.attr('id');  
  var eId     = $('#'+trId+' td.identity').html();
  var eName   = $('#'+trId+' td.name span').html();


       
  var input = $('<input id="newName" type="text" value="'+eName+'" size="10" maxlength="32" />');
  
//  if (! tr.children("td.name").children("input")) //Only append if it doesn't already contain an input element
    tr.children("td.name").html("");
    tr.children("td.name").append("<span>"+eName+"</span>");
    tr.children("td.name").children("span").hide();
    tr.children("td.name").append(input);
  
  input.focusout(function() 
  {
		$.ajax({
  	  type: 'POST',
  	  url: '../ajax.php',
  	  data: ({act: 'act'+tableId, op: 2, id: eId, name: eName, newName: $("#newName").val() }),  //op: 3 = UPDATE
      beforeSend: function() { disableNomenclator(tableId, true); },
  	  success: function() 
      { 
         tr.children("td.name").children("span").html($("#newName").val());
         tr.children("td.name").children("span").show();
         tr.children("td.name").children("input").remove();           
         disableNomenclator(tableId, false);
      }  
    });  
  });    
}


function delFromNom(tr)
{ 
  var trId    = tr.attr('id');  
  var table   = tr.parent().parent();  
  var tableId = table.attr('id');  
  var eId     = $('#'+trId+' td.identity').html();
  var eName   = $('#'+trId+' td.name span').html();

  $('#'+tableId+' tr#'+trId).click(function() 
  {  
    if(confirm('Sigur doriti sa stergeti aceasta entitate?'))
    {
      var eTableTr = $(this);
  		$.ajax({
    	  type: 'POST',
    	  url: '../ajax.php',
    	  data: ({act: 'act'+tableId, op: 3, id: eId, name: eName }),  //op: 3 = DELETE
        beforeSend: function() { disableNomenclator(tableId, true); },
    	  success: function() { eTableTr.remove(); disableNomenclator(tableId, false); }  
      });
    }        
  });
}


function disableNomenclator(tableId, disable)
{
  if (disable)
  {
    $('#'+tableId+' tr th span').hide();
    $('#'+tableId+'_loadingBar').show();
//    $('#'+tableId+' tr td').fadeTo('fast', 0.3);
    $('#'+tableId+' tr td.operations').children().css("visibility", "hidden");
  }
  else
  {
    $('#'+tableId+' tr th span').show();
    $('#'+tableId+'_loadingBar').hide();
//    $('#'+tableId+' tr td').fadeTo('fast', 1);
    $('#'+tableId+' tr td.operations').children().css("visibility", "visible");
  }
}



//=========================== DropDowns ========================================



function populateDD(selectId, jsonArray, teaser)
{
  select = $('#'+selectId);

  select.children().remove(); //Remove all children
  
  if (teaser!=null)
  {
    var firstOpt = $('<option>');
    firstOpt.val("-1");
    firstOpt.html(teaser); 
    select.append(firstOpt);
  }
  
  $.each(jsonArray, 
    function(id, name)
    {      
      var opt = $('<option>');
      opt.val(id);
      opt.html(name);
      
      select.append(opt);        
    }
  );
}



//==================== Multiple selectors ======================================



function populateMultipleSelector(divId, flEnt, crtEnt, eIdParent1, eIdParent2)
{
  var div         = $('#'+divId);
  var title       = div.children("pre").html();
  div.html('<pre>'+title+'</pre>');   //Remove all previous content

  var mainTable   = $('<table class="multipleSelector">');  
  var tr1         = $("<tr>");
  var tr2         = $("<tr>");
  var th          = $("<th>");
  var td1         = $("<td>"); 
  var td2         = $("<td>");
  var td3         = $("<td>");

  var select      = $('<select id="_sel_'+divId+'">');
  select.attr("multiple", "multiple");
  
  //Iterate flEnt and append to select
  $.each(flEnt, 
    function(id, name)
    {      
      var opt = $('<option>');
      opt.val(id);
      opt.html(name);
      
      select.append(opt);        
    }
  );

  //Create a btn to insert multiple selects in the box
  var btn = $('<input type="button" id="btn_'+divId+'" value="&gt;">'); 
  
  //Create a table placeholder for a simple nomenclator and later call the function to populate it
  var simpleNom = $('<table id="_sn_'+divId+'" class="nomenclator"><tr><th colspan="3"><span>Valori</span></th></tr></table>');

  th.attr("colspan", 3);
  th.html(title);
  tr1.html(th);
  mainTable.append(tr1);

  td1.html(select);
  tr2.append(td1);
  td2.html(btn);
  tr2.append(td2);
  td3.html(simpleNom);
  tr2.append(td3);  
  mainTable.append(tr2);

  div.append(mainTable);

  //Handlers
  populateSimpleNomenclator('_sn_'+divId, crtEnt, eIdParent1, eIdParent2); //Populate the table placeholder with a simpe nomenclator
  btn.click(function() { insertIntoSimpleNomenclator(select.val(), '_sel_'+divId, '_sn_'+divId, eIdParent1, eIdParent2); } );    //bind to btn click event

}



//==================== Simple nomenclator ======================================



function populateSimpleNomenclator(tableId, jsonArray, eIdParent1, eIdParent2)
{
  var table = $('#'+tableId);

  //Iterate jsonArray and append a TR in table
  $.each(jsonArray, 
    function(index, value)
    {      
      var tr            = $('<tr id="'+tableId+'_'+index+'">');      
      var tdIndex       = $('<td class="identity">'+index+'</td>');
      var tdName        = $('<td class="name"><span>'+value+'</span></td>');
      var tdOperations  = $('<td class="operations">');
      var imgDel        = $('<img src="../img/btn_delete.png" width="12" height="12" alt="" title="Sterge entitate" />');
      imgDel.click(function() { delFromSimpleNom($(this).parent().parent(), eIdParent1, eIdParent2); } );      
      tdOperations.append(imgDel);

      tr.append(tdIndex);
      tr.append(tdName);
      tr.append(tdOperations);
      
      table.append(tr);
    }
  );
  
  //Create a loading bar which is going to be visible only when AJAX is performing
  var loadingBar = $('<img id="'+tableId+'_loadingBar" src="../img/loading_bar.gif" width="50" height="10" alt="loading...">');
  $('#'+tableId+' th').append(loadingBar);
  loadingBar.hide();
  
  $("td.identity").css("display", "none");  
}


function delFromSimpleNom(tr, eIdParent1, eIdParent2)
{ 
  var trId    = tr.attr('id');  
  var table   = tr.parent().parent();  
  var tableId = table.attr('id');  
  var eId     = $('#'+trId+' td.identity').html();
  var eName   = $('#'+trId+' td.name span').html();

  $('#'+tableId+' tr#'+trId).click(function() 
  {  
    if(confirm('Sigur doriti sa stergeti aceasta entitate?'))
    {
      var eTableTr = $(this);
  		$.ajax({
    	  type: 'POST',
    	  url: '../ajax.php',
    	  data: ({act: 'act'+tableId, op: 3, id: eId, name: eName, idParent1: eIdParent1, idParent2: eIdParent2 }),  //op: 3 = DELETE
        beforeSend: function() { disableNomenclator(tableId, true); },
    	  success: function() { eTableTr.remove(); disableNomenclator(tableId, false); $("td.identity").css("display", "none"); }  
      });
    }        
  });
}


function insertIntoSimpleNomenclator(selVals, selId, tableId, eIdParent1, eIdParent2)
{
  var table = $('#'+tableId);
  var sel   = $('#'+selId);
  var i  	= 0;
  
    for (i=0; i<selVals.length; i++)
    {
      var eName = sel.children('option[value='+selVals[i]+']').text();     
      var tr            = $('<tr id="'+tableId+'_'+selVals[i]+'">');      
      var tdIndex       = $('<td class="identity">'+selVals[i]+'</td>');
      var tdName        = $('<td class="name"><span>'+eName+'</span></td>');
      var tdOperations  = $('<td class="operations">');
           
      tdOperations.append('<img src="../img/btn_delete.png" width="12" height="12" alt="" title="Sterge entitate" onclick="delFromSimpleNom($(this).parent().parent()); return false;">'); 
       
      tr.append(tdIndex);
      tr.append(tdName);
      tr.append(tdOperations);
      
  		$.ajax({
    	  type: 'POST',
    	  url: '../ajax.php',
    	  async: false,
    	  data: ({act: 'act'+tableId, op: 1, id: selVals[i], name: eName, idParent1: eIdParent1, idParent2: eIdParent2 }),  //op: 1 = INSERT
        beforeSend: function() { disableNomenclator(tableId, true); },
    	  success: function() { disableNomenclator(tableId, false); table.append(tr); $("td.identity").css("display", "none"); }  
      });        
    }
}
