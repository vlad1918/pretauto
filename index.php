<?php

include_once 'include/classes.php';
include_once 'include/common.php';

error_reporting(E_ALL);

//Do some checks for SEO pages and searches
$isSpecific       = false;
$isFullySpecific  = false;
$vehicle          = null;

if (! empty($_GET['brand']) && SEO_FRIENDLY==true )
{
  $brand = Brand::readName($_GET['brand']);
  $model = Model::readName($brand->getId(), $_GET['model']);
  $year  = Year::readName($_GET['year']);
  
  $keywords = ','.$brand->getName().','.$model->getName().',Benzina,'.$year->getName();  
  $description = $brand->getName().' '.$model->getName();

  if ($brand->getId() && $model->getId() && $year->getId())    
    $isSpecific = true;
    
  if (! empty($_GET['idFuel']))
  {
    $fuel     = Fuel::read($_GET['idFuel']);
    $distance = intval($_GET['distance']);
  
    if ($fuel->getId() && $distance)
      $isFullySpecific = true;    
  }  
  
  if ($isSpecific && ! $isFullySpecific)     //If only isSpecific then this is a SEO link so we need to create the vehicle object by our selfs
  {
    $idFuel           = 1; //Default to gas    
    $distances        = SmartDistance::parseMediumDistance($year->getId());   
    $distanceStart    = $distances[0];
    $distanceEnd      = $distances[1];
    $idDistanceStart  = $distanceStart->getId();
    $idDistanceEnd    = $distanceEnd->getId();

    $vehicle  = Vehicle::read($brand->getId(), $model->getId(), $year->getId(), $idFuel, $idDistanceStart, $idDistanceEnd);      
  }  
  else if ($isSpecific && $isFullySpecific)  //If isFullySpecific then this is a link created by the user when he submitted a form and we can create the whole vehicle object with his data
  {
    $distances        = SmartDistance::parseDistance($year->getId(), $distance);   
    $distanceStart    = $distances[0];
    $distanceEnd      = $distances[1];
    $idDistanceStart  = $distanceStart->getId();
    $idDistanceEnd    = $distanceEnd->getId();

    $vehicle  = Vehicle::read($brand->getId(), $model->getId(), $year->getId(), $fuel->getId(), $idDistanceStart, $idDistanceEnd);    
  }
  
}

include_once 'include/header.inc.php';
/*********************************** HTML *************************************/
?>
 
<div id="left"><?php echo $bannerLeft; ?></div>

<div id="center">
  <form action="<?php $_SERVER['PHP_SELF']?>" method="post">
    <div>
      <table id="form">        
        <tr>
          <th colspan="3">Completează datele de mai jos şi apoi apasă pe butonul "Evaluare" pentru a obţine o estimare a preţului auto în funcţie de modelul vehiculului, anul fabricaţiei, tipul de carburant şi distanţa parcursă:</th>     
        </tr>
        <tr>
          <td class="label">Model maşină:</td>
          <td>
            <select id="brand"><option value="-1">- Selectati marca -</option></select>
          </td>
          <td>
            <select id="model"><option value="-1">- Selectati model -</option></select>
          </td>      
        </tr>
        <tr>
          <td class="label">Alte detalii:</td>
          <td>
            <select id="year" class="criteria"><option value="-1">- Selectati anul fabricatiei -</option></select>
          </td>
          <td>
            <select id="fuel" class="criteria"><option value="-1">- Selectati carburantul -</option></select>
          </td>      
        </tr>        
        <tr>
          <td class="label">Kilometraj:</td>
          <td colspan="2">
           <div id="distanceBar">             
              <div id="dist"></div>            
              <span id="km"></span> <span>km</span> 
           </div>          
          </td>   
        </tr>          
        
        <tr>        
          <td colspan="3">
            <div id="submitWrapper">
              <input type="button" id="submit" value="Evaluare" />
            </div>
          </td>    
        </tr>            
      </table>    
    </div>  
  </form>   
  <div id="result">
    <img src="img/loading_bar.gif" alt="loading..." width="50" height="10" />
    <h1>Preţul estimat al autovehiculului este: <b><!-- popul8 l8er --></b> euro</h1>
  </div>
</div>

<div id="right"><?php echo $bannerRight; ?></div>


<script type="text/javascript">
<!--

$(document).ready(
  function()
  { 
    disableInputs();
      
    $("#dist").slider({
    	range: "min",
      value: 15000,
    	min: 0,
    	max: 300000,
    	step: 5000,
    	slide: function(event, ui) {
    		$("#km").html(ui.value);	
    	}
    });
    $("#km").html($("#dist").slider("value"));   
  }
);

$("#brand").ready(      //Populates the brands drop down
  function()
  {  
		$.ajax({
  	  type: 'POST',
  	  url: 'ajax.php',
  	  data: ({act: 'listbrand'}),
  	  dataType: 'json',
  	  async: false,
      beforeSend: function() { $('#brand').attr('disabled', true); disableInputs(); },
  	  success: function(jsonArray) { $('#brand').attr('disabled', false); populateDD('brand', jsonArray, '- Selectati marca -'); }
	});      
});

$('#brand').change(    //Populates the model drop down
  function()
  {  
		$.ajax({
  	  type: 'POST',
  	  url: 'ajax.php',
  	  data: ({act: 'listmodel', idBrand: $("#brand").val()}),
  	  dataType: 'json',
  	  async: false,
      beforeSend: function() { $('#model').attr('disabled', true); disableInputs(); },
  	  success: function(jsonArray) { $('#model').attr('disabled', false); populateDD('model', jsonArray, '- Selectati model -'); }
	});    
});

$('#model').change(    //Populates the year, fuel and distance drop downs 
  function()
  { 
    //Gets years 
		$.ajax({
  	  type: 'POST',
  	  url: 'ajax.php',
  	  data: ({act: 'listyear', idBrand: $("#brand").val(), idModel: $("#model").val()}),
  	  dataType: 'json',
  	  async: false,
      beforeSend: function() { $('#year').attr('disabled', true); },
  	  success: function(jsonArray) { $('#year').attr('disabled', false); populateDD('year', jsonArray, '- Selectati anul fabricatiei -'); }
	  }); 
    //Gets fuels   	  
		$.ajax({
  	  type: 'POST',
  	  url: 'ajax.php',
  	  data: ({act: 'listfuel', idBrand: $("#brand").val(), idModel: $("#model").val()}),
  	  dataType: 'json',
  	  async: false,
      beforeSend: function() { $('#fuel').attr('disabled', true); },
  	  success: function(jsonArray) { $('#fuel').attr('disabled', false); populateDD('fuel', jsonArray, '- Selectati carburantul -'); }  	  
	  }); 
    //Display distance bar   	  
    $('#distanceBar').css("visibility", "visible");        
});

$('.criteria').change(    //If all ok, disables submit button
  function()
  {
    if (isOk4Submit())
      $('#submit').attr('disabled', false);
    else
      $('#submit').attr('disabled', true);   
  }
);


$("select").change(
	function()
	{
		if ($("#result h1").is(":visible"))
			$("#result h1").hide();
	}
);


$('#submit').click(    //Estimates a price for the vehicle
  function()
  {
    
    <?php if(SEO_FRIENDLY==false) { ?>
    		$.ajax({
      	  type: 'POST',
      	  url: 'ajax.php',
      	  data: ({act: 'getPrice', idBrand: $("#brand").val(), idModel: $("#model").val(), idYear: $("#year").val(), idFuel: $("#fuel").val(), distance: $("#dist").slider("value") }),
          beforeSend: function() { $('#result h1').hide(); $('#result img').show(); },
      	  success: function(price) 
          {                                 
            if(jQuery.trim(price).length > 2)
            {
              $('#result h1 b').html(price);
            }
            else
            {
              $('#result h1').html('Ne cerem scuze dar momentan nu avem informaţii despre acest vehicul');
            }        
            
            $('#result h1').show();
            $('#result img').hide();                
          }
    	 });
    
    <?php } ?>   

    <?php if(SEO_FRIENDLY==true) { ?> 
	    var brandName = $("#brand option:selected").text();
	    var modelName = $("#model option:selected").text();
	    var yearName  = $("#year option:selected").text();
	    var idFuel    = $("#fuel").val();
	    var distance  = $("#dist").slider("value");        
      
      	window.location.href=""+brandName+"-"+modelName+"-"+yearName+"-"+idFuel+"-"+distance+".html";          
    <?php } ?>        
});


//== Needed functions ==

//Disables inputs that need parents
function disableInputs()
{
  $('#model').attr('disabled', true);
  $('#model').val(0);  
  $('#year').attr('disabled', true);
  $('#year').val(0);
  $('#fuel').attr('disabled', true);
  $('#fuel').val(0);
  $('#distanceBar').css("visibility", "hidden");
  $('#submit').attr('disabled', true);
}


function isOk4Submit()
{
  var isOk = false;

  if ( $('#year').val() > 0 && $('#fuel').val() > 0) 
    isOk = true;

  return isOk;      
}

<?php /*====== If a $vehicle object exists autocomplete the form and send it ===*/ 
if ($vehicle) {  ?>

$("#brand").ready(      //Populates the brands drop down
  function()
  {  
    var idBrand     = <?php echo $vehicle->getBrand()->getId(); ?>;
    var idModel     = <?php echo $vehicle->getModel()->getId(); ?>;
    var idYear      = <?php echo $vehicle->getYear()->getId(); ?>;
    var idFuel      = <?php echo $vehicle->getFuel()->getId(); ?>;
    var intDistance = <?php if (! empty($_GET['distance'])) echo $_GET['distance']; else echo $vehicle->getDistanceStart()->getName(); ?>;
  
    $("#brand option[value="+idBrand+"]").attr("selected", true);
    $("#brand").trigger("change");
    $("#model option[value="+idModel+"]").attr("selected", true);
    $("#model").trigger("change");
    $("#year option[value="+idYear+"]").attr("selected", true);
    $("#year").trigger("change");
    $("#fuel option[value="+idFuel+"]").attr("selected", true);
    $("#fuel").trigger("change");
    $("#dist").slider("value", intDistance)
    $("#km").html(intDistance);
    
    $.ajax({
  	  type: 'POST',
  	  url: 'ajax.php',
  	  data: ({act: 'getPrice', idBrand: $("#brand").val(), idModel: $("#model").val(), idYear: $("#year").val(), idFuel: $("#fuel").val(), distance: $("#dist").slider("value") }),
      beforeSend: function() { $('#result h1').hide(); $('#result img').show(); },
  	  success: function(price) 
      {    	                 
        if(jQuery.trim(price).length > 2)
        {                        
          $('#result h1 b').html(price);
        }
        else
        {
          $('#result h1').html('Ne cerem scuze dar momentan nu avem informaţii despre acest vehicul');
        }        
        
        $('#result h1').show();
        $('#result img').hide(); 
      }
    }); 
    
  }
);

<?php }?>

-->
</script> 

<?php
include_once 'include/footer.inc.php';
?>