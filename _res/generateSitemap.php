<?php
/**
 *
 * Generates the XML sitemap for the site 
 *
 */  
 
include_once '../include/common.php';
include_once '../include/crawler.class.php';
include_once '../include/classes.php';

error_reporting(E_ALL && ~E_NOTICE);

//==============//
$xml = '<?xml version="1.0" encoding="UTF-8"?'.'>';
$xml .= "\n".'<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
//==============//
$log = '';
//==============//
$cnt = 0;
$start = time();
$brands = Brand::getList();
foreach ($brands as $brand)
{
  $models = Model::getList($brand->getId());   //Get brand models
  foreach ($models as $model)
  {
    $years = Year::getList($brand->getId(), $model->getId());    
    foreach ($years as $year)
    {
      $cnt++;
      $url  = "http://www.pret-auto.ro/{$brand->getName()}-{$model->getName()}-Benzina-{$year->getName()}.html";
      $date = date('Y-m-d');
      $time = date('H:i:s');
      
      
      $idFuel           = 1; //Default to gas    
      $distances        = SmartDistance::parseMediumDistance($year->getId());   
      $distanceStart    = $distances[0];
      $distanceEnd      = $distances[1];
      $idDistanceStart  = $distanceStart->getId();
      $idDistanceEnd    = $distanceEnd->getId();
      
      $vehicle  = Vehicle::read($brand->getId(), $model->getId(), $year->getId(), $idFuel, $idDistanceStart, $idDistanceEnd);  
      $cache = cacheVehicle($vehicle, true);

      if (! $cache)
        $log .= "\nVehicle: ".$vehicle->toString()." could not be cached";
      
      if ($cache)
        {
          $xml .= "\n<url>";
          $xml .= "\n<loc>{$url}</loc>";
          $xml .= "\n<lastmod>{$date}T{$time}z</lastmod>";                 
          $xml .= "\n<changefreq>monthly</changefreq>";
          $xml .= "\n<priority>0.5</priority>";
          $xml .= "\n</url>";
        }     
    } 
  }
}

$xml .= "\n".'</urlset>';

file_put_contents("sitemap.xml", trim($xml));
file_put_contents("log.txt", trim($log));

echo "\nFinished searching".$cnt." objects in ".((time() - $start)/60)." minutes!\n";

//=================== Functions ======================
/**
 * Checks if a vehicle can be crowled and inserts it in the database. 
 * If a price cannot be obtained then returns false 
 */ 
function cacheVehicle($vehicle, $cacheInDb)
{
    $retVal = false;
    
    $scan = new ScanAlyzer($vehicle, ScanAlyzer::AUTO_RO);
    $price = round($scan->getNewPrice());
  
    if ($price!=null)
    {
      $retVal = $price;
      if($cacheInDb)
      {
        $vehicle->setPrice($price);
        $vehicle->insert();
      } 
    }  
    return $retVal;
}