// ==UserScript==
// @name           AutoRo Model Extractor
// @description    A script to extract car models on www.auto.ro
// @version        1.0.1
// @author         scazi
// @include        *.auto.ro/*
// ==UserScript==

var marca = document.getElementById('marci_anunt');
var home = document.getElementById('container');


var btn = document.createElement('INPUT');
btn.type ='button';
btn.value ='Vezi modele dupa ce selectezi o marca!';
btn.style.display ='block';
btn.style.position ='fixed';
btn.style.top ='0';
btn.style.left ='0';

btn.setAttribute('onclick',"alert(document.getElementById('modeleAnunt').innerHTML);"); 

home.appendChild(btn);