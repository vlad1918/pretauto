<?php
include_once '../include/classes.php';
include_once '../include/common.php';
include_once '../include/header.inc.php';
?>

<table id="brand" class="left nomenclator">
  <tr>  
    <th colspan="3"><span>Mărci</span></th>
  </tr> 
</table>

<table id="model" class="left nomenclator">
  <tr>  
    <th colspan="3"><span>Modele</span> <span></span></th>
  </tr>
  <tr>  
    <td colspan="3">-- Selectati o marca --</td>
  </tr>   
</table>

<div class="clear"></div>

<table id="year" class="left nomenclator">
  <tr>  
    <th colspan="3"><span>Ani</span></th>
  </tr> 
</table>

<table id="fuel" class="left nomenclator">
  <tr>  
    <th colspan="3"><span>Carburanţi</span></th>
  </tr> 
</table>

<table id="distance" class="left nomenclator">
  <tr>  
    <th colspan="3"><span>Distanţe</span></th>
  </tr> 
</table>

<div class="clear"></div>

<script type="text/javascript">
<!--

$(document).ready(      //Populates brands nomenclator
  function()
  {
		$.ajax({          //Populate #brand
  	  type: 'POST',
  	  url: '../ajax.php',
  	  data: ({act: 'listbrand'}),
  	  dataType: 'json',
  	  success: function(jsonArray) { populateNomenclator($('#brand'), jsonArray, true, '<img src="../img/btn_fork.png" width="12" height="12" alt="Y" title="Editeaza modele" onclick="displayModel($(this).parent().parent()); return false;">'); },
	  }); 
   
    $.ajax({        //Populate #year
  	  type: 'POST',
  	  url: '../ajax.php',
  	  data: ({act: 'listfullyear'}),
  	  dataType: 'json',
  	  success: function(jsonArray) { populateNomenclator($('#year'), jsonArray, false); }
	  }); 
    
    $.ajax({        //Populate #fuel
  	  type: 'POST',
  	  url: '../ajax.php',
  	  data: ({act: 'listfullfuel'}),
  	  dataType: 'json',
  	  success: function(jsonArray) { populateNomenclator($('#fuel'), jsonArray, true); }
	  }); 
    
    $.ajax({        //Populate #distance
  	  type: 'POST',
  	  url: '../ajax.php',
  	  data: ({act: 'listfulldistance'}),
  	  dataType: 'json',
  	  success: function(jsonArray) { populateNomenclator($('#distance'), jsonArray, false); }
	  });  
});

function displayModel(tr)
{
  var selBrandId    = tr.children("td.identity").html(); 
  var selBrandName  = tr.children("td.name").children("span").html();

  $('#model th').children("span:last").html(selBrandName); //Add model name to table header
  $('#model td').remove(); //Remove all previous TDs
  $('#model').attr("idParent", selBrandId); //add idBrand as idParent
  
  $.ajax({        //Populate #model
	  type: 'POST',
	  url: '../ajax.php',
	  data: ({act: 'listmodel', idBrand: selBrandId}),
	  dataType: 'json',
	  beforeSend: function() { disableNomenclator('model', true); },
	  success: function(jsonArray) { populateNomenclator($('#model'), jsonArray, true); disableNomenclator('model', false); }
  });

}

-->
</script> 

<?php
include_once '../include/footer.inc.php';
?>
