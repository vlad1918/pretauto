<?php 
include_once '../include/classes.php';
include_once '../include/common.php';
include_once '../include/header.inc.php';
?>

<select id="year"><option value="-1">- Selectati anul -</option></select>

<div id="bindEntity">
  <div id="distance" class="box"><pre>Distante</pre></div>
  <div class="clear"></div>
</div>

<script type="text/javascript">
<!--

$("#year").ready(      //Populates the year drop down
  function()
  {  
		$.ajax({
  	  type: 'POST',
  	  url: '../ajax.php',
  	  data: ({act: 'listfullyear'}),
  	  dataType: 'json',
      beforeSend: function() { $('#year').attr('disabled', true); },
  	  success: function(jsonArray) { $('#year').attr('disabled', false); populateDD('year', jsonArray, '- Selectati anul -'); }
	});    
});


$('#year').change(    //Populates the distance multiple selector 
  function()
  {   
    if ($('#year').val()!=-1)
    {
        $("#bindEntity").children("div").show();
        
        //Gets distances   	  
    		var flDistances;  //fl = full list
    		var crtDistances; //crt = current list
        $.ajax({   //get all distances from nomenclator
      	  type: 'POST',
          url: '../ajax.php',
          data: ({act: 'listfulldistance'}),
      	  dataType: 'json',
//        beforeSend: function() { $('#distance').attr('disabled', 'disabled'); },
      	  success: function(jsonArray) 
          { 
            flDistances   = jsonArray;
            eIdYear  = $("#year").val();
            $.ajax({        //get distances for this type of vehicle only
      	      type: 'POST',
      	      url: '../ajax.php',
      	      data: ({act: 'listsmartdistance', idYear: eIdYear}),
          	  dataType: 'json',
          	  success: function(jsonArray) 
              { 
                crtDistances = jsonArray;
                populateMultipleSelector('distance', flDistances, crtDistances, eIdYear, null);
              }
        	  });        
          }  	  
    	  });        
           	  
    }
    else
    {
      $("#bindEntity").children("div").hide();
    }  
  });


-->
</script> 

<?php
include_once '../include/footer.inc.php';
?>





