<?php 
include_once '../include/classes.php';
include_once '../include/common.php';
include_once '../include/header.inc.php';
?>

<select id="brand"><option value="-1">- Selectati marca -</option></select>
<select id="model"><option value="-1">- Selectati model -</option></select>

<div id="bindEntity">
  <div id="year" class="box"><pre>Ani</pre></div>
  <div id="fuel" class="box"><pre>Carburanti</pre></div>
  <div id="distance" class="box"><pre>Distante</pre></div>
  <div class="clear"></div>
</div>

<script type="text/javascript">
<!--

$('#model').attr("disabled", true);

$("#brand").ready(      //Populates the brands drop down
  function()
  {  
		$.ajax({
  	  type: 'POST',
  	  url: '../ajax.php',
  	  data: ({act: 'listbrand'}),
  	  dataType: 'json',
      beforeSend: function() { $('#brand').attr('disabled', true); $('#model').attr("disabled", true); },
  	  success: function(jsonArray) { $('#brand').attr('disabled', false); populateDD('brand', jsonArray, '- Selectati marca -'); }
	});    
});

$('#brand').change(    //Populates the model drop down
  function()
  { 
    if ($('#brand').val()!=-1)
    { 
      $("#bindEntity").children("div").show();
  		$.ajax({
    	  type: 'POST',
    	  url: '../ajax.php',
    	  data: ({act: 'listmodel', idBrand: $("#brand").val()}),
    	  dataType: 'json',
        beforeSend: function() { $('#model').attr('disabled', 'disabled'); },
    	  success: function(jsonArray) { $('#model').attr('disabled', false); populateDD('model', jsonArray, '- Selectati model -'); }
  	   });
    }
    else
    {
      $("#bindEntity").children("div").hide();
      $('#model').attr('disabled', 'disabled');
      $('#model').val(0);
    }        
});

$('#model').change(    //Populates the year, fuel multiple selectors
  function()
  {   
    if ($('#model').val()!=-1)
    {
        $("#bindEntity").children("div").show();
        
        //Gets years   	  
    		var flYears;  //fl = full list
    		var crtYears; //crt = current list
        $.ajax({   //get all years from nomenclator
      	  type: 'POST',
          url: '../ajax.php',
          data: ({act: 'listfullyear'}),
      	  dataType: 'json',
//        beforeSend: function() { $('#year').attr('disabled', 'disabled'); },
      	  success: function(jsonArray) 
          { 
            flYears   = jsonArray;
            eIdBrand  = $("#brand").val();
            eIdModel  = $("#model").val();
            $.ajax({        //get years for this type of vehicle only
      	      type: 'POST',
      	      url: '../ajax.php',
      	      data: ({act: 'listyear', idBrand: eIdBrand, idModel: eIdModel}),
          	  dataType: 'json',
          	  success: function(jsonArray) 
              { 
                crtYears = jsonArray;
                populateMultipleSelector('year', flYears, crtYears, eIdBrand, eIdModel);
              }
        	  });        
          }  	  
    	  });
    	  
        //Gets fuels   	  
    		var flFuels;  //fl = full list
    		var crtFuels; //crt = current list
        $.ajax({   //get all fuels from nomenclator
      	  type: 'POST',
          url: '../ajax.php',
          data: ({act: 'listfullfuel'}),
      	  dataType: 'json',
//        beforeSend: function() { $('#fuel').attr('disabled', 'disabled'); },
      	  success: function(jsonArray) 
          { 
            flFuels   = jsonArray;
            eIdBrand  = $("#brand").val();
            eIdModel  = $("#model").val();
            $.ajax({        //get fuels for this type of vehicle only
      	      type: 'POST',
      	      url: '../ajax.php',
      	      data: ({act: 'listfuel', idBrand: eIdBrand, idModel: eIdModel}),
          	  dataType: 'json',
          	  success: function(jsonArray) 
              { 
                crtFuels = jsonArray;
                populateMultipleSelector('fuel', flFuels, crtFuels, eIdBrand, eIdModel);
              }
        	  });        
          }  	  
    	  }); 
        
/*        
        //Gets distances   	  
    		var flDistances;  //fl = full list
    		var crtDistances; //crt = current list
        $.ajax({   //get all distances from nomenclator
      	  type: 'POST',
          url: '../ajax.php',
          data: ({act: 'listfulldistance'}),
      	  dataType: 'json',
//        beforeSend: function() { $('#distance').attr('disabled', 'disabled'); },
      	  success: function(jsonArray) 
          { 
            flDistances   = jsonArray;
            eIdBrand  = $("#brand").val();
            eIdModel  = $("#model").val();
            $.ajax({        //get distances for this type of vehicle only
      	      type: 'POST',
      	      url: '../ajax.php',
      	      data: ({act: 'listdistance', idBrand: eIdBrand, idModel: eIdModel}),
          	  dataType: 'json',
          	  success: function(jsonArray) 
              { 
                crtDistances = jsonArray;
                populateMultipleSelector('distance', flDistances, crtDistances, eIdBrand, eIdModel);
              }
        	  });        
          }  	  
    	  });        
*/
           	  
    }
    else
    {
      $("#bindEntity").children("div").hide();
    }  
  });


-->
</script> 

<?php
include_once '../include/footer.inc.php';
?>





