<?php

include_once 'include/classes.php';
include_once 'include/crawler.class.php';
include_once 'include/common.php';

/**
 * All AJAX request are done with the POST method
 * All AJAX request must have a variable called act
 */  

$act = $_POST['act'];

switch($act)
{
  case 'listbrand':  
    
    $brands = Brand::getList();
    $jsBrands = array();
      
    foreach($brands as $brand)
    {
       $jsBrands[$brand->getId()] = $brand->getName(); 
    }
         
    echo json_encode($jsBrands);    
    
    break;

  case 'actbrand':  
    
    $op       = $_POST['op'];
    $id       = $_POST['id'];
    $name     = $_POST['name'];
    
    $brand = new Brand($id, $name);
    
    if($op==1)
      $brand->insert();
          
    else if ($op==2)
    {
      $newName  = $_POST['newName'];
      $brand->update($newName);
    }
    
    else if ($op==3)    
      $brand->delete();
    
    break;

  case 'listmodel':  
    
    $idBrand  = intval($_POST['idBrand']);  
    $models   = Model::getList($idBrand);
    $jsModels = array();  
      
    foreach($models as $model)
    {
       $jsModels[$model->getId()] = $model->getName(); 
    }
         
    echo json_encode($jsModels);    
    
    break;


  case 'actmodel':  
    
    $op       = $_POST['op'];
    $id       = $_POST['id'];
    $name     = $_POST['name'];
    $idBrand  = $_POST['idParent'];
    
    $model = new Model($id, $idBrand, $name);
    
    if($op==1)
      $model->insert();
          
    else if ($op==2)
    {
      $newName  = $_POST['newName'];
      $model->update($newName);
    }
    
    else if ($op==3)    
      $model->delete();
    
    break;

  case 'listyear':  
    
    $idBrand = intval($_POST['idBrand']); 
    $idModel = intval($_POST['idModel']);  
    $years = Year::getList($idBrand, $idModel);
    $jsYears = array();
      
    foreach($years as $year)
    {
       $jsYears[$year->getId()] = $year->getName(); 
    }
         
    echo json_encode($jsYears);    
    
    break;

  case 'listfullyear':  
    
    $years = Year::getFullList();
    $jsYears = array();
      
    foreach($years as $year)
    {
       $jsYears[$year->getId()] = $year->getName(); 
    }
         
    echo json_encode($jsYears);    
    
    break;

  case 'actyear':  
    
    $op       = $_POST['op'];
    $id       = $_POST['id'];
    $name     = $_POST['name'];
    
    $year = new Year($id, $name);
    
    if($op==1)
      $year->insert();
          
    else if ($op==2)
    {
      $newName  = $_POST['newName'];
      $year->update($newName);
    }
    
    else if ($op==3)    
      $year->delete();
    
    break;


  case 'act_sn_year':  
    
    $op       = $_POST['op'];
    $id       = $_POST['id'];
    $name     = $_POST['name'];
    $idBrand  = $_POST['idParent1'];
    $idModel  = $_POST['idParent2'];
    
    $year = new Year($id, $name);
    
    if($op==1)
      $year->insertLink($idBrand, $idModel);
    
    else if ($op==3)    
      $year->deleteLink($idBrand, $idModel);
    
    break;

  case 'listfuel':  
    
    $idBrand = intval($_POST['idBrand']); 
    $idModel = intval($_POST['idModel']);
    $fuels   = Fuel::getList($idBrand, $idModel); 
    $jsFuels = array();
      
    foreach($fuels as $fuel)
    {
       $jsFuels[$fuel->getId()] = $fuel->getName(); 
    }
         
    echo json_encode($jsFuels);    
    
    break;

  case 'listfullfuel':  
    
    $fuels = Fuel::getFullList();
    $jsFuels = array();
      
    foreach($fuels as $fuel)
    {
       $jsFuels[$fuel->getId()] = $fuel->getName(); 
    }
         
    echo json_encode($jsFuels);    
    
    break;

  case 'actfuel':  
    
    $op       = $_POST['op'];
    $id       = $_POST['id'];
    $name     = $_POST['name'];
    
    $fuel = new Fuel($id, $name);
    
    if($op==1)
      $fuel->insert();
          
    else if ($op==2)
    {
      $newName  = $_POST['newName'];
      $fuel->update($newName);
    }
    
    else if ($op==3)    
      $fuel->delete();
    
    break;

  case 'act_sn_fuel':  
    
    $op       = $_POST['op'];
    $id       = $_POST['id'];
    $name     = $_POST['name'];
    $idBrand  = $_POST['idParent1'];
    $idModel  = $_POST['idParent2'];
    
    $fuel = new Fuel($id, $name);
    
    if($op==1)
      $fuel->insertLink($idBrand, $idModel);
    
    else if ($op==3)    
      $fuel->deleteLink($idBrand, $idModel);
    
    break;

  case 'listdistance':  
    
    $idBrand = intval($_POST['idBrand']); 
    $idModel = intval($_POST['idModel']);  
    $distances = Distance::getList($idBrand, $idModel);
    $jsDistances = array();
      
    foreach($distances as $distance)
    {
       $jsDistances[$distance->getId()] = $distance->getName(); 
    }
         
    echo json_encode($jsDistances);    
    
    break;

  case 'listfulldistance':  
    
    $distances = Distance::getFullList();
    $jsDistances = array();
      
    foreach($distances as $distance)
    {
       $jsDistances[$distance->getId()] = $distance->getName(); 
    }
         
    echo json_encode($jsDistances);    
    
    break;

  case 'listsmartdistance':  

    $idYear = intval($_POST['idYear']);  
    $distances = SmartDistance::getList($idYear);
    $jsDistances = array();
      
    foreach($distances as $distance)
    {
       $jsDistances[$distance->getId()] = $distance->getName(); 
    }
         
    echo json_encode($jsDistances);    
    
    break;

  case 'actdistance':  
    
    $op       = $_POST['op'];
    $id       = $_POST['id'];
    $name     = $_POST['name'];
    
    $distance = new Distance($id, $name);
    
    if($op==1)
      $distance->insert();
          
    else if ($op==2)
    {
      $newName  = $_POST['newName'];
      $distance->update($newName);
    }
    
    else if ($op==3)    
      $distance->delete();
    
    break;

  case 'act_sn_distance':  
    
    $op       = $_POST['op'];
    $id       = $_POST['id'];
    $name     = $_POST['name'];
    $idYear   = $_POST['idParent1'];
    
    $distance       = new Distance($id, $name);
    $year           = Year::read($idYear);
    $smartDistance  = new SmartDistance($year, $distance);
    
    if($op==1)
      $smartDistance->insert();
    
    else if ($op==3)    
      $smartDistance->delete();
    
    break;

  case 'getPrice':
  
    $idBrand          = intval($_POST['idBrand']); 
    $idModel          = intval($_POST['idModel']);
    $idYear           = intval($_POST['idYear']); 
    $idFuel           = intval($_POST['idFuel']);    
    $distance         = intval($_POST['distance']);
    
    $distances        = SmartDistance::parseDistance($idYear, $distance);   
    $distanceStart    = $distances[0];
    $distanceEnd      = $distances[1];
    $idDistanceStart  = $distanceStart->getId();
    $idDistanceEnd    = $distanceEnd->getId();
    
    //Try to get the vehicle object from the database
    $vehicle  = Vehicle::readFast($idBrand, $idModel, $idYear, $idFuel, $idDistanceStart, $idDistanceEnd);     
    $price = 0; 
     
    if ($vehicle->getPrice()==null) //Cache miss
    {
        $newVehicle  = Vehicle::read($idBrand, $idModel, $idYear, $idFuel, $idDistanceStart, $idDistanceEnd); //Read vehicle full
        $scan = new ScanAlyzer($newVehicle, ScanAlyzer::AUTO_RO);        
        $price = round($scan->getNewPrice());
     
        //If $price == 0 search for another similar $vehicle object that has a suitable price
        if ($price == 0)
        {
       
          //Get the min and max distance and search a price using those 
          $distances        = SmartDistance::getMinMaxDistance($idYear);             
          $newVehicle->setDistanceStart($distances[0]);
          $newVehicle->setDistanceEnd($distances[1]);
        
          $price = $newVehicle->calcPrice(); //first check in cache

          if ($price == null)
          {
          	$scan = new ScanAlyzer($newVehicle, ScanAlyzer::AUTO_RO);
          	$price = round($scan->getNewPrice());
          }
          	
          if ($price == 0) //If price is still == 0 then fine tune the years of the vehicle
          {        
              
              $allYears = Year::getFullList();            
              $iMinYear = intval($allYears[0]->getName());
              $iMaxYear = intval($allYears[count($allYears)-1]->getName());
              
              $testVehicle = $newVehicle;
              $year = Year::read($idYear);
              $iYear = intval($year->getName());           
              for ($i=1; $i<=5; $i++)
              {
                $iYear = $iYear - $i;
                $testYear = Year::readName($iYear);
                $testVehicle->setYear($testYear);

                $price = $testVehicle->calcPrice(); //first check in cache
                if($price!=null)
                {
                	$newVehicle = $testVehicle;
                	break;
                }
                //else continue scanning!
                $scan = new ScanAlyzer($testVehicle, ScanAlyzer::AUTO_RO);        
                $price = round($scan->getNewPrice());                
                if ($price!=0 && $iYear>=$iMinYear) //We have finally found a price :)
                {
                  $testVehicle->setYear($testYear);
                  $newVehicle = $testVehicle;                
                  break;
                }
                
                
                $iYear = $iYear + (2*$i);
                $testYear = Year::readName($iYear);
                $testVehicle->setYear($testYear);
                
                $price = $testVehicle->calcPrice(); //first check in cache
                if($price!=null)
                {
                	$newVehicle = $testVehicle;
                	break;
                }
                //else continue scanning!
                $scan = new ScanAlyzer($testVehicle, ScanAlyzer::AUTO_RO);        
                $price = round($scan->getNewPrice());
                if ($price!=0 && $iYear<=$iMaxYear) //We have finally found a price :)
                {
                  $testVehicle->setYear($testYear);
                  $newVehicle = $testVehicle;
                  break;
                }
                              
              }              
          }          
        }                        
        
        $newVehicle->setPrice($price);
        $newVehicle->insert();  //cache the new vehicle in the local database        
        unset($newVehicle);    
    }
    else //Cache hit
    {            
      //Check to see if cache is not outdated      
      $now        = time();
      $cacheTime  = strtotime($vehicle->getTime());
      $life       = abs($now - $cacheTime);
      
      if ( $life < CACHE_LIFE ) //Cache is not outdated and does not need to be renewed  
        $price = $vehicle->getPrice();
      else //Cache needs to ne renewed
      {
        $newVehicle  = Vehicle::read($idBrand, $idModel, $idYear, $idFuel, $idDistanceStart, $idDistanceEnd); //Read vehicle full
        $scan = new ScanAlyzer($newVehicle, ScanAlyzer::AUTO_RO);        
        $price = round($scan->getNewPrice());
        $newVehicle->setPrice($price);
        $newVehicle->update($price);  //update the outdated price        
        unset($newVehicle);              
      }
    }
    
    echo $price;
    
    break;


  default:
    echo '!!!default!!!';  
    break;
 
}



?>