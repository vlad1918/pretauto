<?php

include_once 'include/common.php';
include_once 'include/header.inc.php';
include_once 'include/func.inc.php';

/** Get data from the form passed through POST **/
$userInfo['name'] = trim($_POST['name']);
$userInfo['phone'] = $_POST['phone'];
$userInfo['email'] = trim($_POST['email']);
$userInfo['details'] = trim($_POST['details']);
$userInfo['invocation'] = intval($_POST['invocation']);

if( $userInfo['invocation'] == 2 ) // The user has submitted the form
{
  if (! $userInfo['details']) // If the user hasn't written any message
    $message = '<span class="red">Vă rugăm să introduceţi un mesaj cu solicitarea dumneavoastră.</span>';
  elseif (! $userInfo['phone'] && ! $userInfo['email'] )  // If the user hasn't entered a phone nor an email address
    $message = '<span class="red">Vă rugăm să introduceţi un număr de telefon sau o adresă de e-mail pentru a vă putea răspunde.</span>';
  elseif ( $userInfo['email'] && ! isValidEmail($userInfo['email']) )  // If the e-mail is not valid
    $message = '<span class="red">Vă rugăm să introduceţi o adesă de e-mail validă.</span>';
  else
  {
      
    //Send a mail
    $to = MAIL_TO;
    $subject = MAIL_SUBJECT;
    
    $body .= "\n".'Nume: '.$userInfo['name'];
    $body .= "\n".'Telefon: '.$userInfo['phone'];
    $body .= "\n".'E-mail: '.$userInfo['email'];
    $body .= "\n".'------'."\n\n";
    $body .= "\n".$userInfo['details'];
    $body .= "\n";
  	
  	if(! mail($to, $subject, convertRoChars($body)))
  		$message = '<span class="red">Mesajul nu a putut fi trimis! Vă rugăm să ne contactaţi la adresa <a href="mailto:'.MAIL_TO.'">'.MAIL_TO.'</a></span>';
    else
    {  
      $message = '<span class="green"><b>Mesaj trimis cu succes. Vă mulţumim.</b></span>';
      $userInfo = array(); //Clear the inputs
    }
  }
}
  
/*************************** Display the form *********************************/

?>

<div id="left"><?php echo $bannerLeft; ?></div>

<div id="center">
  <div id="contactBox">
    <p>Ne puteţi contacta folosind formularul de mai jos.
    <br>Nu uitaţi să introduceţi un număr de telefon sau o adresă de e-mail pentru a vă putea contacta.</p>
    <form id="contact" action="contact.php" method="post"><div>
    <input type="hidden" name="invocation" value="2" />
    <table>
    	<tr><td class="label">Nume:</td><td class="text"><input class="text" type="text" name="name" value="<?php echo(htmlspecialchars($userInfo['name'])); ?>" size="45" maxlength="64" /></td></tr>
    	<tr><td class="label">Telefon:</td><td class="text"><input class="text" type="text" name="phone" value="<?php echo(htmlspecialchars($userInfo['phone'])); ?>" size="45" maxlength="64" /></td></tr>
    	<tr><td class="label">E-mail:</td><td class="text"><input class="text" type="text" name="email" value="<?php echo(htmlspecialchars($userInfo['email'])); ?>" size="45" maxlength="64" /></td></tr>
    	<tr><td class="label">Mesaj:</td><td class="text"><textarea class="text" cols="40" name="details" rows="6"><?php echo(htmlspecialchars($userInfo['details'])); ?></textarea></td></tr>
      <tr><td></td><td class="message"><?php echo($message); ?></td></tr>
    	<tr><td colspan="2" class="btnSend"><input id="btnSend" type="submit" value="Trimiteţi" /></td></tr>		
    </table>
    </div></form>
  </div>
</div>

<div id="right"><?php echo $bannerRight; ?></div>

<?php
include_once 'include/footer.inc.php';
?>