<?php
/**
 * Classes for describing vehicles
 *
 */    

 //--- Helper classes (entities) ---
class Brand
{
  private $id;     //int
  private $name;   //String
  
  public function Brand($id, $name)
  {
    $this->id     = $id;
    $this->name   = $name;
  }
  
  //--- PERSISTENCE ---
  public static function read($id)
  {
    global $db;
    $query = "SELECT id, name FROM `".TABLE_BRAND."` WHERE id={$id}";
    $result = $db->query($query);   
    $row = $result->fetch_assoc();
    $name = $row['name'];
    
    return new Brand($id, $name);   
  } 

  public static function readName($nameBrand)
  {
    global $db;
    $query = "SELECT id, name FROM `".TABLE_BRAND."` WHERE name='{$nameBrand}'";
    $result = $db->query($query);
    $row = $result->fetch_assoc();
    $id = $row['id'];
    
    return new Brand($id, $nameBrand);   
  }
  
  public static function getList()
  {
    global $db;
    $query = "SELECT id, name FROM `".TABLE_BRAND."`";
    $result = $db->query($query);
    $brands = array(); /* Brand */  
    while ($row = $result->fetch_assoc())
    {
      $brands[] = new Brand($row['id'], $row['name']);
    }
    
    return $brands;    
  }  

  public function insert()
  {
    global $db;
    $query = "INSERT INTO `".TABLE_BRAND."` VALUES({$this->getId()}, '{$this->getName()}')";              
    $result = $db->query($query);  
  } 

  public function update($newName)
  {
    global $db;
    $query = "UPDATE `".TABLE_BRAND."` SET name='{$newName}' WHERE id={$this->getId()}";       
    $result = $db->query($query);  
  }  
  
  public function delete()
  {
    global $db;
    $query = "DELETE FROM `".TABLE_BRAND."` WHERE id={$this->getId()} LIMIT 1";   
    $result = $db->query($query);  
  }  
  
  //--- GETTERS ---
  public function getId()
  {
     return $this->id;
  }
  
  public function getName()
  {
     return $this->name;  
  }
}

class Model
{
  private $id;      //int
  private $idBrand; //int
  private $name;    //String
  
  public function Model($id, $idBrand, $name)
  {
    $this->id       = $id;
    $this->idBrand  = $idBrand;
    $this->name     = $name;
  }
  
  //--- PERSISTENCE ---
  public static function read($idBrand, $idModel)
  {
    global $db;
    $query = "SELECT id, name FROM `".TABLE_MODEL."` WHERE id={$idModel} AND idBrand=".$idBrand;
    $result = $db->query($query);
    $row = $result->fetch_assoc();
    return new Model($row['id'], $idBrand, $row['name']);   
  }    

  public static function readName($idBrand, $nameModel)
  {
    global $db;
    $query = "SELECT id, name FROM `".TABLE_MODEL."` WHERE idBrand=".intval($idBrand)." AND name='{$nameModel}'";
    $result = $db->query($query);
    $row = $result->fetch_assoc();
    return new Model($row['id'], $idBrand, $row['name']);   
  }
   
  public static function getList($idBrand)
  {
    global $db;
    $query = "SELECT id, name FROM `".TABLE_MODEL."` WHERE idBrand=".$idBrand;
    $result = $db->query($query);
    $models = array(); /* Model */  
    while ($row = $result->fetch_assoc())
    {
      $models[] = new Model($row['id'], $idBrand, $row['name']);
    }
    
    return $models;    
  }  
    
  public function insert()
  {
    global $db;
    $query = "INSERT INTO `".TABLE_MODEL."` VALUES({$this->getId()}, {$this->getIdBrand()}, '{$this->getName()}')";             
    $result = $db->query($query);  
  } 

  public function update($newName)
  {
    global $db;
    $query = "UPDATE `".TABLE_MODEL."` SET name='{$newName}' WHERE id={$this->getId()}";    
    $result = $db->query($query);  
  }  
  
  public function delete()
  {
    global $db;
    $query = "DELETE FROM `".TABLE_MODEL."` WHERE id={$this->getId()} LIMIT 1";   
    $result = $db->query($query);  
  } 
   
  //--- GETTERS ---
  public function getId()
  {
     return $this->id;
  }

  public function getIdBrand()
  {
     return $this->idBrand;
  }
  
  public function getName()
  {
     return $this->name;  
  }
}

abstract class Criteria
{
  private $id;     //int
  private $name;     //int or String
  
  public function Criteria($id, $name)
  {
    $this->id     = $id;
    $this->name   = $name;
  }
  
  //--- GETTERS ---
  public function getId()
  {
     return $this->id;
  }
  
  public function getName()
  {
     return $this->name;
  }
  
}

class Year extends Criteria
{ 
  //--- PERSISTENCE ---
  public static function read($idYear)
  {
    global $db;
    $query = "SELECT `".TABLE_YEAR."`.id, `".TABLE_YEAR."`.year FROM `".TABLE_YEAR."` WHERE id={$idYear}";
    $result = $db->query($query);
    $row = $result->fetch_assoc();
    return new Year($row['id'], $row['year']);  
  }

  public static function readName($nameYear)
  {
    global $db;
    $query = "SELECT `".TABLE_YEAR."`.id, `".TABLE_YEAR."`.year FROM `".TABLE_YEAR."` WHERE year='{$nameYear}'";
    $result = $db->query($query);
    $row = $result->fetch_assoc();
    return new Year($row['id'], $row['year']);  
  }
  
  public static function getList($idBrand, $idModel)
  {
    global $db;
    $query = "SELECT `".TABLE_YEAR."`.id, `".TABLE_YEAR."`.year FROM `".TABLE_MODEL_HAS_YEAR."` INNER JOIN `".TABLE_YEAR."` ON idYear=id WHERE idBrand={$idBrand} AND idModel={$idModel}";
    $result = $db->query($query);
    $years = array(); /* Year */  
    while ($row = $result->fetch_assoc())
    {
      $years[] = new Year($row['id'], $row['year']);
    }
    
    return $years;    
  } 

  public static function getFullList()
  {
    global $db;
    $query = "SELECT * FROM `".TABLE_YEAR."`";
    $result = $db->query($query);
    $years = array(); /* Year */  
    while ($row = $result->fetch_assoc())
    {
      $years[] = new Year($row['id'], $row['year']);
    }
    
    return $years;    
  }
  
  public function insert()
  {
    global $db;
    $query = "INSERT INTO `".TABLE_YEAR."`(year) VALUES('".parent::getName()."')";               
    $result = $db->query($query);  
  } 

  public function update($newName)
  {
    global $db;
    $query = "UPDATE `".TABLE_YEAR."` SET year='{$newName}' WHERE id=".parent::getId();             
    $result = $db->query($query);  
  }  
  
  public function delete()
  {
    global $db;
    $query = "DELETE FROM `".TABLE_YEAR."` WHERE id=".parent::getId()." LIMIT 1";   
    $result = $db->query($query);  
  }  

  public function insertLink($idBrand, $idModel)
  {
    global $db;
    $query = "INSERT INTO `".TABLE_MODEL_HAS_YEAR."` VALUES({$idBrand}, {$idModel}, ".parent::getId().")";                  
    $result = $db->query($query);  
  }
  
  public function deleteLink($idBrand, $idModel)
  {
    global $db;
    $query = "DELETE FROM `".TABLE_MODEL_HAS_YEAR."` WHERE idYear=".parent::getId()." AND idBrand={$idBrand} AND idModel={$idModel} LIMIT 1";     
    $result = $db->query($query);  
  }  
  
}

class Fuel extends Criteria
{  
  //--- PERSISTENCE ---
  public static function read($idFuel)
  {
    global $db;
    $query = "SELECT `".TABLE_FUEL."`.id, `".TABLE_FUEL."`.type FROM `".TABLE_FUEL."` WHERE id={$idFuel}";
    $result = $db->query($query);
    $row = $result->fetch_assoc();
    return new Fuel($row['id'], $row['type']);  
  }  
  
  public static function getFullList()
  {
    global $db;
    $query = "SELECT * FROM `".TABLE_FUEL."`";
    $result = $db->query($query);
    $fuels = array(); /* Fuel */  
    while ($row = $result->fetch_assoc())
    {
      $fuels[] = new Fuel($row['id'], $row['type']);
    }
    
    return $fuels;    
  }
  
  public static function getList($idBrand, $idModel)
  {
    global $db;
    $query = "SELECT `".TABLE_FUEL."`.id, `".TABLE_FUEL."`.type FROM `".TABLE_MODEL_HAS_FUEL."` INNER JOIN `".TABLE_FUEL."` ON idFuel=id WHERE idBrand={$idBrand} AND idModel={$idModel}";
    $result = $db->query($query);
    $fuels = array(); /* Fuel */  
    while ($row = $result->fetch_assoc())
    {
      $fuels[] = new Fuel($row['id'], $row['type']);
    }
    
    return $fuels;    
  }
      
  public function insert()
  {
    global $db;
    $query = "INSERT INTO `".TABLE_FUEL."` VALUES(".parent::getId().", '".parent::getName()."')";                   
    $result = $db->query($query);  
  } 

  public function update($newName)
  {
    global $db;
    $query = "UPDATE `".TABLE_FUEL."` SET type='{$newName}' WHERE id=".parent::getId();             
    $result = $db->query($query);  
  }  
  
  public function delete()
  {
    global $db;
    $query = "DELETE FROM `".TABLE_FUEL."` WHERE id=".parent::getId()." LIMIT 1";     
    $result = $db->query($query);  
  }

  public function insertLink($idBrand, $idModel)
  {
    global $db;
    $query = "INSERT INTO `".TABLE_MODEL_HAS_FUEL."` VALUES({$idBrand}, {$idModel}, ".parent::getId().")";                 
    $result = $db->query($query);  
  }
  
  public function deleteLink($idBrand, $idModel)
  {
    global $db;  
    $query = "DELETE FROM `".TABLE_MODEL_HAS_FUEL."` WHERE idFuel=".parent::getId()." AND idBrand={$idBrand} AND idModel={$idModel} LIMIT 1";           
    $result = $db->query($query);  
  }
  
}

class Distance extends Criteria
{
  //--- PERSISTENCE ---
  public static function read($idDistance)
  {
    global $db;
    $query = "SELECT `".TABLE_DISTANCE."`.id, `".TABLE_DISTANCE."`.distance FROM `".TABLE_DISTANCE."` WHERE id={$idDistance}";
    $result = $db->query($query);
    $row = $result->fetch_assoc();
    return new Distance($row['id'], $row['distance']);  
  }  
  
  public static function getFullList()
  {
    global $db;
    $query = "SELECT * FROM `".TABLE_DISTANCE."`";
    $result = $db->query($query);
    $distances = array(); /* Distance */  
    while ($row = $result->fetch_assoc())
    {
      $distances[] = new Distance($row['id'], $row['distance']);
    }
    
    return $distances;    
  }
  
  public static function getList($idBrand, $idModel)
  {
    global $db;
    $query = "SELECT `".TABLE_DISTANCE."`.id, `".TABLE_DISTANCE."`.distance FROM `".TABLE_MODEL_HAS_DISTANCE."` INNER JOIN `".TABLE_DISTANCE."` ON idDistance=id WHERE idBrand={$idBrand} AND idModel={$idModel}";
    $result = $db->query($query);
    $distances = array(); /* Distance */  
    while ($row = $result->fetch_assoc())
    {
      $distances[] = new Distance($row['id'], $row['distance']);
    }  
    return $distances;    
  }
        
  public function insert()
  {
    global $db;
    $query = "INSERT INTO `".TABLE_DISTANCE."` VALUES(".parent::getId().", '".parent::getName()."')";               
    $result = $db->query($query);  
  } 

  public function update($newName)
  {
    global $db;
    $query = "UPDATE `".TABLE_DISTANCE."` SET distance='{$newName}' WHERE id=".parent::getId();             
    $result = $db->query($query);  
  }  
  
  public function delete()
  {
    global $db;
    $query = "DELETE FROM `".TABLE_DISTANCE."` WHERE id=".parent::getId()." LIMIT 1";   
    $result = $db->query($query);  
  }
}


class SmartDistance
{
  private $year;      //Year
  private $distance;  //Distance  
  
  public function SmartDistance($year, $distance)
  {
    $this->year     = $year;
    $this->distance = $distance;
    

  }
  
  //--- PERSISTENCE ---
  public static function parseDistance($idYear, $iDistance)
  {
    $allDistances = SmartDistance::getList($idYear);    
        
    $tempDistanceStart  = 0;
    $tempDistanceEnd    = 0;
    
    $nbAllDistances = count($allDistances);
    
    for($i=0; $i<$nbAllDistances; $i++)
    {
      if ($iDistance >= $allDistances[$i]->getName())
      {
        if ($i < $nbAllDistances-1) //When not in the last incrementation there is a valid i+1 so we can form an interval
        {
          $tempDistanceStart  = $allDistances[$i];
          $tempDistanceEnd    = $allDistances[$i+1];
        }
        else //We can not increment i so we must fallback to i+1
        {
          $tempDistanceStart  = $allDistances[$i-1];
          $tempDistanceEnd    = $allDistances[$i];          
        }
      }                           
    }

    return array($tempDistanceStart, $tempDistanceEnd);  
  } 

  public static function parseMediumDistance($idYear)
  {
    $allDistances = SmartDistance::getList($idYear);        
    $nbAllDistances = count($allDistances);    
    if ($nbAllDistances==2)
    	$half = 0;
    else
    	$half = floor($nbAllDistances/2);

    return array($allDistances[$half], $allDistances[$half+1]);  
  } 
  
  public static function getMinMaxDistance($idYear)
  {
    $allDistances = SmartDistance::getList($idYear);        
    $nb = count($allDistances); 
    
    return array($allDistances[0], $allDistances[($nb-1)]);    
  }
  
  public static function getList($idYear)
  {
    global $db;
    $query = "SELECT `".TABLE_DISTANCE."`.id, `".TABLE_DISTANCE."`.distance FROM `".TABLE_YEAR_HAS_DISTANCE."` INNER JOIN `".TABLE_DISTANCE."` ON idDistance=id WHERE idYear={$idYear}";
    $result = $db->query($query);
    $distances = array(); /* Distance */ 
    while ($row = $result->fetch_assoc())
    {
      $distances[] = new Distance($row['id'], $row['distance']);
    }  
    return $distances;    
  }  
  
  public function insert()
  {
    global $db;
    $query = "INSERT INTO `".TABLE_YEAR_HAS_DISTANCE."`(idYear, idDistance) VALUES({$this->getYear()->getId()}, {$this->getDistance()->getId()})";               
    $result = $db->query($query);  
  }
  
  public function delete()
  {
    global $db;
    $query = "DELETE FROM `".TABLE_YEAR_HAS_DISTANCE."` WHERE idYear={$this->getYear()->getId()} AND idDistance={$this->getDistance()->getId()} LIMIT 1";   
    $result = $db->query($query);  
  }
  
  //--- GETTERS ---
  public function getYear()
  {
     return $this->year;
  }
  
  public function getDistance()
  {
     return $this->distance;
  }
}



//--- Main class ---

class Vehicle
{
  private $brand;         //Brand
  private $model;         //Model
  private $year;          //Year
  private $fuel;          //Fuel
  private $distanceStart; //Distance
  private $distanceEnd;   //Distance
  private $price;         //int
  private $time;          //int (UNIX Timestamp)
 
  public function Vehicle($brand, $model, $year, $fuel, $distanceStart, $distanceEnd, $price, $time)
  {
    $this->brand          = $brand;           //Brand
    $this->model          = $model;           //Model
    $this->year           = $year;            //Year
    $this->fuel           = $fuel;            //Fuel
    $this->distanceStart  = $distanceStart;   //Distance
    $this->distanceEnd    = $distanceEnd;     //Distance
    $this->price          = $price;           //int
    $this->time           = $time;            //int (UNIX Timestamp)
  } 

  public static function read($idBrand, $idModel, $idYear, $idFuel, $idDistanceStart, $idDistanceEnd)
  {
    $vehicle = Vehicle::readFast($idBrand, $idModel, $idYear, $idFuel, $idDistanceStart, $idDistanceEnd);
    
    $brand = Brand::read($idBrand);
    $vehicle->setBrand($brand);
    
    $model = Model::read($idBrand, $idModel);
    $vehicle->setModel($model);
    
    $year = Year::read($idYear);
    $vehicle->setYear($year);    
    
    $fuel = Fuel::read($idFuel);
    $vehicle->setFuel($fuel); 
    
    $distanceStart = Distance::read($idDistanceStart);
    $vehicle->setDistanceStart($distanceStart); 

    $distanceEnd = Distance::read($idDistanceEnd);
    $vehicle->setDistanceEnd($distanceEnd); 
    
    return $vehicle;
  } 
 
  public static function readFast($idBrand, $idModel, $idYear, $idFuel, $idDistanceStart, $idDistanceEnd)
  {
    global $db;    
    $query = "SELECT price, time FROM `".TABLE_VEHICLE."` WHERE idBrand={$idBrand} AND idModel={$idModel} AND idYear={$idYear} AND idFuel={$idFuel} AND idDistanceStart={$idDistanceStart} AND idDistanceEnd={$idDistanceEnd}";
    $result = $db->query($query);
    $row = $result->fetch_assoc();
    $price = $row['price'];
    $time  = $row['time'];
    
    return new Vehicle(null, null, null, null, null, null, $price, $time);
  } 
  
  public function calcPrice()
  {
  	global $db;
  	$query = "SELECT price FROM `".TABLE_VEHICLE."` WHERE idBrand={$this->getBrand()->getId()} AND idModel={$this->getModel()->getId()} AND idYear={$this->getYear()->getId()} AND idFuel={$this->getFuel()->getId()} AND idDistanceStart={$this->getDistanceStart()->getId()} AND idDistanceEnd={$this->getDistanceEnd()->getId()}";
  	$result = $db->query($query);
  	$row = $result->fetch_assoc();
  	return $row['price'];  	
  }
  
  public function insert()
  {
    global $db;
    $query = "INSERT INTO `".TABLE_VEHICLE."`(idBrand, idModel, idYear, idFuel, idDistanceStart, idDistanceEnd, price) VALUES({$this->getBrand()->getId()}, {$this->getModel()->getId()}, {$this->getYear()->getId()}, {$this->getFuel()->getId()}, {$this->getDistanceStart()->getId()}, {$this->getDistanceEnd()->getId()}, {$this->getPrice()})";               
    $result = $db->query($query);  
  } 

  public function update($newPrice)
  {
    global $db;
    $query = "UPDATE `".TABLE_VEHICLE."` SET price='{$newPrice}' WHERE idBrand={$this->getBrand()->getId()} AND idModel={$this->getModel()->getId()} AND idYear={$this->getYear()->getId()} AND idFuel={$this->getFuel()->getId()} AND idDistanceStart={$this->getDistanceStart()->getId()} AND idDistanceEnd={$this->getDistanceEnd()->getId()}";             
    $result = $db->query($query);  
  } 
  
  public function toString() //A human readable representation of the object
  {
    return $this->getBrand()->getName()." ".$this->getModel()->getName()." ".$this->getYear()->getName()." ".$this->getFuel()->getName()." ".$this->getDistanceStart()->getName()."-".$this->getDistanceEnd()->getName();
  }
  
  //--- GETTERS ---
  public function getBrand()
  {
     return $this->brand;
  }

  public function getModel()
  {
     return $this->model;
  }

  public function getYear()
  {
     return $this->year;
  }

  public function getFuel()
  {
     return $this->fuel;
  }

  public function getDistanceStart()
  {
     return $this->distanceStart;
  } 

  public function getDistanceEnd()
  {
     return $this->distanceEnd;
  }   
  public function getPrice()
  {
     return $this->price;
  }
  
  public function getTime()
  {
     return $this->time;
  }
  
  //--- SETTERS ---
  public function setBrand($brand)
  {
     $this->brand = $brand;
  }

  public function setModel($model)
  {
    $this->model = $model;
  }

  public function setYear($year)
  {
    $this->year = $year;
  }

  public function setFuel($fuel)
  {
     $this->fuel = $fuel;
  }

  public function setDistanceStart($distanceStart)
  {
     $this->distanceStart = $distanceStart;
  } 

  public function setDistanceEnd($distanceEnd)
  {
     $this->distanceEnd = $distanceEnd;
  }  

  public function setPrice($price)
  {
     $this->price = $price;
  }
    
}



?>