<?php

/**
 *
 * ScanAlyzer v1.0 
 *
 */  

include_once 'classes.php';
include_once 'func.inc.php';

class ScanAlyzer
{
  private $vehicle;   // Vehicle object
  private $target;    // string; Example: AUTO_RO, ...

  private $newPrice;  // int; The new proce obtained after crawling

  private $userAgent  = 'Mozilla/5.0 (Windows NT 5.1; rv:5.0) Gecko/20100101 Firefox/5.0)';

  const AUTO_RO = 'AUTO_RO';
  
  /**
   * Constructor for the ScanAlyzer
   */     
  public function ScanAlyzer($vehicle, $target)
  {
    $this->vehicle  = $vehicle;
    $this->target   = $target; 
    
    //Valid strings for $target 
    $validTargets = array(AUTO_RO);
     
    //If target is unknown throw an exception    
    if (! in_array($target, $validTargets))
      throw new Exception("Target='{$target}' is unknown");   
    
    //Depending on $target crawl using diffrent functions
    if ($target==AUTO_RO)
      $this->newPrice = $this->crawlAutoRoFgc();
    
  }

    /**
   * Crawls www.auto.ro site and returns medium price for the vehicle using file_get_contents() and avoids using cURL
   *    
   * @return: int $mediumPrice;     
   */
   private function crawlAutoRoFgc()
   {  
    //Define local constants
    $brand = strtolower($this->getVehicle()->getBrand()->getName());
    $brand = str_replace(array('-', ' '), '_', $brand);
    $model = strtolower($this->getVehicle()->getModel()->getName());
    $model = str_replace(array('-', ' '), '_', $model);
    $year  = $this->getVehicle()->getYear()->getName();  
    $fuel  = strtolower(convertRoChars($this->getVehicle()->getFuel()->getName()));

    $url   = "http://www.auto.ro/{$brand}-{$model}/leasing~2-an~{$year}_{$year}-km~{$this->getVehicle()->getDistanceStart()->getName()}_{$this->getVehicle()->getDistanceEnd()->getName()}-carburant~{$fuel}.html";
    $page  = file_get_contents($url);  
    
    //Parse $page and make calculate medium price for vehicles
    $matchedPrices    = array();    
    $matchedDistances = array();
    
    $patternPrices    = '|<span class=\"list_pret pd_tp\">(.+) EUR</span>|';    //<span class="list_pret pd_tp">3.700 EUR</span>
    $patternDistances = '|<span class=\"list_km pd_tp\">(.+) km </span>|';      //<span class="list_km pd_tp">150.000 km </span>
     
    preg_match_all ($patternPrices,     $page, $matchedPrices);
    preg_match_all ($patternDistances,  $page, $matchedDistances);

    //Remove '.' in order to create valid integers
    $matchedPrices    = str_replace('.', '', $matchedPrices[1]);
    $matchedDistances = str_replace('.', '', $matchedDistances[1]);

    $nbPrices = count($matchedPrices);

    if ($nbPrices != count($matchedDistances))
      throw new Exception("Matched prices and Matched distances for ".$url." do not have the same number of results");
    
    $totalPrice = 0;   
    for($i=0; $i<$nbPrices; $i++)
    {
      $totalPrice += $matchedPrices[$i];
    }  

    $mediumPrice = 0;
    if ($nbPrices > 0)
      $mediumPrice = $totalPrice/$nbPrices;

    return $mediumPrice;   
   } 

  /**
   * Crawls www.auto.ro site and returns medium price for the vehicle
   *    
   * @return: int $mediumPrice;     
   */
   //@Deprecated!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   private function crawlAutoRo()
   {  
    //Define local constants
    define('URL', 'http://www.auto.ro/cautare-avansata'); // http://www.auto.ro/cautare-avansata    
    $postVars = "id_marca={$this->getVehicle()->getBrand()->getId()}&id_model={$this->getVehicle()->getModel()->getId()}&start_price=&end_price=&start_year={$this->getVehicle()->getYear()->getName()}&end_year={$this->getVehicle()->getYear()->getName()}&start_km={$this->getVehicle()->getDistanceStart()->getName()}&end_km={$this->getVehicle()->getDistanceEnd()->getName()}&carburant={$this->getVehicle()->getFuel()->getId()}&avans_maxim=&rata_maxima=&leasing=0&categorie=2";
    
    //CURL Magic
    $ch = curl_init(URL);
    curl_setopt($ch, CURLOPT_POST            ,1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1); 
    curl_setopt($ch, CURLOPT_HEADER          ,0);  // DO NOT RETURN HTTP HEADERS 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);  // RETURN THE CONTENTS OF THE CALL
    curl_setopt($ch, CURLOPT_USERAGENT       ,$this->userAgent);  //Set the agent    
    curl_setopt($ch, CURLOPT_POSTFIELDS      ,$postVars);

    $page = curl_exec($ch);            
    curl_close($ch);
  
    //Parse $page and make calculate medium price for vehicles
    $matchedPrices    = array();    
    $matchedDistances = array();
    
    $patternPrices    = '|<span class=\"list_pret pd_tp\">(.+) EUR</span>|';    //<span class="list_pret pd_tp">3.700 EUR</span>
    $patternDistances = '|<span class=\"list_km pd_tp\">(.+) km </span>|';      //<span class="list_km pd_tp">150.000 km </span>
     
    preg_match_all ($patternPrices,     $page, $matchedPrices);
    preg_match_all ($patternDistances,  $page, $matchedDistances);

    //Remove '.' in order to create valid integers
    $matchedPrices    = str_replace('.', '', $matchedPrices[1]);
    $matchedDistances = str_replace('.', '', $matchedDistances[1]);

    $nbPrices = count($matchedPrices);

    if ($nbPrices != count($matchedDistances))
      throw new Exception("Matched prices and Matched distances for ".POST_VARS." do not have the same number of results");
    
    $totalPrice = 0;   
    for($i=0; $i<$nbPrices; $i++)
    {
      $totalPrice += $matchedPrices[$i];
    }  

    $mediumPrice = 0;
    if ($nbPrices > 0)
      $mediumPrice = $totalPrice/$nbPrices;

    return $mediumPrice;   
   }        


  /**
   * Getter $vehicle
   *   
   * @return: Vehicle $vehicle
   */     
  public function getVehicle()
  {
    return $this->vehicle;
  }

  /**
   * Getter for the new price
   *   
   * @return: int $newPrice
   */     
  public function getNewPrice()
  {
    return $this->newPrice;
  }


}


?>