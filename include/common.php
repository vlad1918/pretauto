<?php
/**
 *
 * Common actions and constants 
 *
 */  
 

define('DEBUGG', false);
define('SEO_FRIENDLY', false);

if (! DEBUGG)
  error_reporting(0); //No error reporting in release mode
else
  error_reporting(E_ERROR | E_WARNING | E_PARSE);

//Define database connection parameters
define('DB_HOSTNAME', 'localhost');
define('DB_DATABASE', '');
define('DB_USERNAME', '');
define('DB_PASSWORD', '');

//SQL Table names
define('TABLE_BRAND',             'brand');
define('TABLE_MODEL',             'model');
define('TABLE_MODEL_HAS_YEAR',    'model_has_year');
define('TABLE_MODEL_HAS_FUEL',    'model_has_fuel');
define('TABLE_MODEL_HAS_DISTANCE','model_has_distance');
define('TABLE_YEAR',              'year');
define('TABLE_FUEL',              'fuel');
define('TABLE_DISTANCE',          'distance');
define('TABLE_YEAR_HAS_DISTANCE', 'year_has_distance');
define('TABLE_VEHICLE',           'vehicle');
define('TABLE_USER',              'user');
define('TABLE_LOGGER',            'logger');

//Define the period that needs to pass in order to renew the cache
define('CACHE_LIFE', 2592000); //30 days

//Create a database object
$db = new mysqli (DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
if ($db->connect_errno) {
    exit('Connect Error: ' . $db->connect_errno);
}

//Prepare database for utf8
$db->query("SET NAMES 'utf8'");

//Contact variables

define('MAIL_TO',             'name@gmail.com');
define('MAIL_SUBJECT',        'Mesaj contact @ evaluare masina');

//Menu
$menu = array(
  '.'                    => 'Evaluare maşină',
//  'cum-functioneaza.php' => 'Cum funcţionează',
//  'contact.php'          => 'Contact'
);


//Banners
$bannerTop    = '<!-- HTML -->';
$bannerLeft   = '<!-- HTML -->';
$bannerRight  = '<!-- HTML -->';






//************************* Administration ***********************************//
$isInAdmin = false;
$scriptName = $_SERVER["SCRIPT_NAME"]; 
$url = dirname($scriptName);
if (substr($url, -1, 1)=='x')
{
  $isInAdmin = true;  

  $tpg = explode('/', $scriptName);
  $crtPage = $tpg[count($tpg)-1];

  //Set menu for admin
  $menu = array(
  'vehicle.php'       => 'Configurare vehicule',
  'nomenclator.php'   => 'Nomenclator entitati',
  'year.php'          => 'Configurare ani',
  'events.php'        => 'Alerte evenimente'
  );

  //Manage user log ins
  session_start();

  if ($_GET['x']==1)
  {
    session_destroy();
    header('Location: index.php');
  }

  if (! $_SESSION['username'] && $crtPage != 'index.php' )
  {
      header('Location: index.php');
  }
  
  if ($_SESSION['username'])
  {    
    $bannerTop    = "<p class=\"alignRight\">Autentificat drept <b>{$_SESSION['username']}</b> [<a href=\"?x=1\">Logout</a>] </p>";
  }
}


?>
 
  
