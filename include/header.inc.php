<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Pret Auto - evaluază preţul maşinii tale</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="imagetoolbar" content="no" />
	<meta http-equiv="MSThemeCompatible" content="yes" />
	<meta name="robots" content="index, follow" />
  <meta name="keywords" content="pret auto,evaluare masina,calculeaza pret masina,evaluator,calculator,estimare,model,distanta parcursa,carburant<?php echo $keywords ?>" />
  <meta name="description" content="Pret Auto - calculeaza pretul masinii tale <?php echo $description ?> in functie de model, anul fabricatiei, distanta parcursa si tipul de carburant folosit" />
<?php if ($isInAdmin) { ?>
  <link rel="StyleSheet" href="../css/style.css" type="text/css" />
  <script type="text/javascript" src="../js/jquery.js"></script>
  <script type="text/javascript" src="../js/nomenclator.js"></script>
<?php } ?>
<?php if (! $isInAdmin) { ?>
  <link rel="StyleSheet" href="css/style.css" type="text/css" />
  <link type="text/css" href="css/start/jquery-ui-1.8.14.custom.css" rel="stylesheet" />	 		
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery-ui.js"></script>
  <script type="text/javascript" src="js/nomenclator.js"></script> 
<?php } ?>
<script type="text/javascript">
(function() {
  var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
  po.src = 'https://apis.google.com/js/plusone.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
})();
</script>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-26684856-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
</head>

<body lang="ro">
<div id="container">

<div id="header">
  <div id="top">
    <a id="home" href="."></a>
    <div id="banner">
       <?php echo $bannerTop; ?>
    </div>
    <div class="clear"></div>
  </div>
  <div id="menu">  
      <?php foreach($menu as $href => $item) { ?>
      <a href="<?php echo $href; ?>"><?php echo $item; ?></a>
      <?php  }?>
      
      <div id="social">
        <div id="googleplus">
           <g:plusone size="medium" href="http://www.pret-auto.ro"></g:plusone>
        </div>      
      </div>      
      <div class="clear"></div>
  </div>
</div><!-- /header -->
<div id="body">