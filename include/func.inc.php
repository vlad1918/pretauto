<?php 
/**
 *
 * Usefull functions
 */

/**
 * Function for verifying e-mail syntax
 *
 * @PARAMS: string $email 
 * RETURN: bool isValidEmail 
 */     
function isValidEmail($email)
{
  return preg_match('/^[[:alpha:]]([[:alnum:]]|[_\\.-])*@([[:alnum:]](([[:alnum:]]|-){0,61}[[:alnum:]])?\\.)+[[:alpha:]]{2,6}$/', $email);
}

/**
 * Function for adding a value into the database
 *
 * @PARAMS: string $value 
 * RETURN: striug $value 
 */ 
function prepare4Db($value)
{
  if (! get_magic_quotes_gpc())
    $value = addslashes($value);
  
  return $value;
}

/**
 * Function for adding a displaying a string value using HTML
 *
 * @PARAMS: string $value 
 * RETURN: striug $value 
 */ 
function prepare4Html($value)
{
  if (! get_magic_quotes_gpc())
    $value = stripslashes($value); 
  $value = htmlspecialchars($value);
  
  return $value;
}

/**
 * Function for sending data to the database
 * 
 * @PARAMS: array $userInfo
 * RETURN: bool affectedTable   
 *
 */  
function sendToDb($userInfo)
{
  $db = new mysqli(HOST, USER, PASSWD, DBNAME);
  if ($db->connect_error)
    exit('Eroare de conexiune la baza de date (' . $mysqli->connect_errno . ') '. $mysqli->connect_error);
  //set collate to utf8_romanian 
  $db->query("SET NAMES 'utf8' COLLATE 'utf8_romanian_ci'");

  //Prepare data for inserting into tabels
  foreach ($userInfo as $field => $value)
    $userInfo[$field] = prepare4Db($value);

  $time = time(); // the UNIX timestamp
  $query = "INSERT INTO contact VALUES('', '{$userInfo['name']}', '{$userInfo['email']}', '{$userInfo['phone']}', '{$userInfo['details']}', {$time}, '{$_SERVER['REMOTE_ADDR']}')";
  
  $db->query($query);
  
  if ($db->affected_rows == 1) // If all is OK then one row should have been affected
    return TRUE;
}


/**
 * Covert UTF8 Romanian characters to their corresponding characters in ISO-8859-1 
 * 
 * @param string $input
 * @return string $output
 * 
 */
function convertRoChars($input)
{
	return (str_replace(array('ă', 'î', 'â', 'ș', 'ț', 'ş', 'ţ', 'Ă', 'Î', 'Â', 'Ș', 'Ț', 'Ş', 'Ţ'), array('a', 'i', 'a', 's', 't', 's', 't', 'A', 'I', 'A', 'S', 'T', 'S', 'T'), $input));	

}


?>
